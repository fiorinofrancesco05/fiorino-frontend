import './App.css'
import Navbar from './components/navbar/navbar'
import Carosello from './components/carosello/carosello';
import UnderCarosello from './components/threeimg/UnderCarosello';
import Ticket from './components/ticket/ticket';
import Form from './components/form/form';
import Footer from './components/footer/footer';

const city = ["Paris", "New York", "San Francisco"]

function App() {

  return (
    <>
      <Navbar nomeStart="Logo" nomeLink1="Home" nomeLink2="Band" nomeLink3="Tour" nomeLink4="More" 
      nomeLink4_1="Action" nomeLink4_2="Second action" nomeLink5="Contact">
      </Navbar>
      <Carosello city1 ={city[0]} city2 ={city[1]} city3 ={city[2]}  ></Carosello>
      <UnderCarosello  city1 ={city[0]} city2 ={city[1]} city3 ={city[2]}></UnderCarosello>
      <Ticket city1 ={city[0]} city2 ={city[1]} city3 ={city[2]}></Ticket>
      <Form></Form>
      <Footer></Footer>



    </>
  );

}

export default App
