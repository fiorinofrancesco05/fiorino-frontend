
import '../cssTotal.css'

export default function Footer(props){

    return(
        <>
     
        <div className="container-fluid bg-dark">

                <div className="text-center">
                    <a href="#">
                    <br/><br/>
                        <svg style={{fill:"white"}} xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor"
                            className="bi bi-arrow-up-short" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5" />
                        </svg>
                        <br/><br/><br/>
                    </a>
                </div>



                <span style={{color:"white"}}>RCS MediaGroup S.p.A.
                    Via Roma, 8 - 20132 Milano.<br/><br/>
                    Copyright 2024 © Tutti i diritti riservati. CF, Partita I.V.A. e <br/> Iscrizione al Registro delle Imprese
                    di Milano n.12086540155.<br/> R.E.A. di Milano: 1524326 Capitale sociale € 270.000.000,00<br/> ISSN
                    2499-3093</span>

                <br/><br/><br/>

                <span style={{color:"rgba(255, 252, 252, 0.404)"}}>
                    Ciao Lorenzo Taverna, anche a te Simone De Meis
                </span>
            </div>

        </>
    )
}




