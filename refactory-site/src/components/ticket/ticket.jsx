import Sanfrancisco from '../../assets/san-francisco1-1080x721.jpg'
import Paris from '../../assets/paris-cityscape-overview-guide.webp'
import Newyork from '../../assets/new-york-storia-monumenti-e-protagonisti-orig.avif'
import '../cssTotal.css'

export default function Ticket(props) {

    return (
        <>

        <br /><br /><br />
            <div className="container-fluid bg-dark text-center mt-5" style={{color:"white"}}>
                <div className="row">
                    <div className="col-md-3"></div>

                    <div className="col-md-6">
                        <p className="spacingLetter mt-4">Tour dates</p>
                        <p>Lorem ipsum we'll play you some music. <br /> Remember to book your ticket!</p>



                        <ul className="list-group">
                            <li className="list-group-item list-group-item-action d-flex align-items-center">
                                September<span className="badge text-bg-danger" style={{marginLeft:"0.5em"}}>Sold out!</span>
                            </li>
                            <li className="list-group-item list-group-item-action d-flex align-items-center">
                                October<span className="badge text-bg-danger" style={{marginLeft:"0.5em"}}>Sold out!</span>
                            </li>
                            <li
                                className="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                                November
                                <span className="badge text-bg-primary rounded-pill">3</span>
                            </li>
                        </ul>

                    </div>

                    <div className="col-md-3"></div>
                </div>


                <div className="container-fluid">
                    <div className="row mt-5">


                        <div className="col-md-3">

                        </div>

                        <div className="col-md-2">
                            <div className="card text-center h-100">
                                <img src={Paris} className="card-img-top card-img" alt="Paris"/>
                                    <div className="card-body">
                                        <h5 className="card-title">{props.city1}</h5>
                                        <p className="card-text">21 November 2015</p>
                                        <a href="#" className="btn btn-dark">Buy ticket</a>
                                    </div>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="card text-center h-100">
                                <img src={Newyork} className="card-img-top card-img"
                                    alt="New York"/>
                                    <div className="card-body">
                                        <h5 className="card-title">{props.city2}</h5>
                                        <p className="card-text">25 November 2015</p>
                                        <a href="#" className="btn btn-dark">Buy ticket</a>
                                    </div>
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div className="card text-center h-100">
                                <img src={Sanfrancisco} className="card-img-top card-img" alt="San Francisco"/>
                                    <div className="card-body">
                                        <h5 className="card-title" id="modifyTitleOval">{props.city3}</h5>
                                        <p className="card-text">29 November 2015</p>
                                        <a href="#" className="btn btn-dark">Buy ticket</a>
                                    </div>
                            </div>
                        </div>

                        <div className="col-md-3">

                        </div>


                    </div>

                </div>

                <br /><br /><br />
            </div>


        </>
    )
}