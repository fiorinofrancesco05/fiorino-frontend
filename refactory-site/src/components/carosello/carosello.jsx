import Sanfrancisco from '../../assets/san-francisco1-1080x721.jpg'
import Paris from '../../assets/paris-cityscape-overview-guide.webp'
import Newyork from '../../assets/new-york-storia-monumenti-e-protagonisti-orig.avif'
import '../cssTotal.css'

export default function Carosello(props){

    return(
        <>
        
        <div id="carouselExampleCaptions" className="carousel slide">
        <div className="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active"
                aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                aria-label="Slide 3"></button>
        </div>
        <div className="carousel-inner">
            <div className="carousel-item active">
                <img src={Paris} className="d-block w-100" style={{height: "95vh"}}
                    alt="paris"/>
                <div className="carousel-caption d-none d-md-block">
                    <h5>{props.city1}</h5>
                    <p>Some representative placeholder content for the first slide.</p>
                </div>
            </div>
            <div className="carousel-item">
                <img src={Newyork} className="d-block w-100"
                    style={{height: "95vh"}} alt="new york"/>
                <div className="carousel-caption d-none d-md-block">
                    <h5>{props.city2}</h5>
                    <p>Some representative placeholder content for the second slide.</p>
                </div>
            </div>
            <div className="carousel-item">
                <img src={Sanfrancisco} className="d-block w-100" style={{height: "95vh"}}
                    alt="san-francisco"/>
                <div className="carousel-caption d-none d-md-block">
                    <h5>{props.city3}</h5>
                    <p>Some representative placeholder content for the third slide.</p>
                </div>
            </div>
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
            data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
            data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
        </button>
    </div>

        </>
    )
}