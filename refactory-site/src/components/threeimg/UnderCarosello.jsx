import Sanfrancisco from '../../assets/san-francisco1-1080x721.jpg'
import Paris from '../../assets/paris-cityscape-overview-guide.webp'
import Newyork from '../../assets/new-york-storia-monumenti-e-protagonisti-orig.avif'
import '../cssTotal.css'

export default function UnderCarosello(props){

    return(
        <>   

        <div className="container-fluid text-center mt-5  mb-5">

            <div className="row">
                <div className="col-md-3"></div>
                <div className="col-md-6">
                    <p className="spacingLetter">THE BAND</p>
                    <p>We love music</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam minus dolorem recusandae amet, ea
                        aliquid veritatis, expedita nostrum saepe maxime, qui natus optio laudantium. Omnis laudantium
                        praesentium delectus qui obcaecati.</p>
                </div>
                <div className="col-md-3"></div>
            </div>


            <div className="row mt-3">
                <div className="col-md-3"></div>
                <div className="col-md-2 ">
                    <div className="text-center Ovalimg">
                        <p className="up3img">{props.city1}</p>
                        <a href="">
                            <img src={Paris} className="rounded-circle" alt="Paris"
                                 style={{width: "70%", height: "70%"}}/>
                        </a>


                    </div>
                </div>
                <div className="col-md-2 ">
                    <div className="text-center Ovalimg">
                        <p className="up3img">{props.city2}</p>
                        <a href="">
                            <img src={Newyork} className="rounded-circle"
                                alt="New york"  style={{width: "70%", height: "70%"}}/>
                        </a>

                    </div>
                </div>
                <div className="col-md-2 ">
                    <div className="text-center Ovalimg">
                        <p className="up3img">{props.city3}</p>
                        <a href="">
                            <img src={Sanfrancisco} className="rounded-circle" alt="San Francisco"
                                style={{width: "70%", height: "70%"}}/>
                        </a>

                    </div>
                </div>
                <div className="col-md-3"></div>
            </div>
            </div>
        </>
    )
}