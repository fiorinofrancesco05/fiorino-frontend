import '../cssTotal.css'

export default function Navbar(props){

    return(

        <>
        
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div className="container-fluid">
            <a className="navbar-brand" href="#">
                {props.nomeStart}
            </a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link active" href="#">{props.nomeLink1}</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">{props.nomeLink2}</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">{props.nomeLink3}</a>
                    </li>
                    <li className="nav-item dropstart">
                        <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            {props.nomeLink4}
                        </a>
                        <ul className="dropdown-menu">
                            <li><a className="dropdown-item" href="#">{props.nomeLink4_1}</a></li>
                            <li><a className="dropdown-item" href="#">{props.nomeLink4_2}</a></li>
                        </ul>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">{props.nomeLink5}</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">
                            <svg style={{fill: "white"}} xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                                <path
                                    d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001q.044.06.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1 1 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0" />
                            </svg>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

        </>

    )

}